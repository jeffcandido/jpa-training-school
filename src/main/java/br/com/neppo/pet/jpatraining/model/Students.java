package br.com.neppo.pet.jpatraining.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table( name = "STUDENTS")
public class Students implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "ID" )
    private Long id;

    @Column( name = "NAME" )
    private String name;

    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "BIRTH_DATE" )
    private Date birth_date;

    @ManyToMany( mappedBy = "students" )
    private List< Teacher > teachers;
}
