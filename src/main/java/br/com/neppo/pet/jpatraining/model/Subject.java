package br.com.neppo.pet.jpatraining.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table( name = "SUBJECT" )
public class Subject implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "ID" )
    private Long id;

    @Column( name = "NAME" )
    private String name;

    @Column( name = "ACTIVE" )
    private boolean active;

    @Temporal( TemporalType.TIMESTAMP )
    @Column( name = "TIME_SCHEDULE" )
    private Date time_schedule;


}
