package br.com.neppo.pet.jpatraining.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table( name = "TEACHER" )
public class Teacher implements Serializable {

    @Id
    @GeneratedValue( strategy = GenerationType.IDENTITY )
    @Column( name = "ID" )
    private Long id;

    @Column( name = "NAME" )
    private String name;

    @ManyToMany
    @JoinTable( name = "TEACHER_STUDENTS",
            joinColumns = @JoinColumn( name = "TEACHER_ID" ),
            inverseJoinColumns = @JoinColumn( name = "STUDENT_ID" )
    )
    private List< Students > students;

}
